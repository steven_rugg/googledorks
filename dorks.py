import html
import requests
from bs4 import BeautifulSoup

class Google_Dork:
    """ Google search engine search strings that allow a user to access sensitive information that 
    may have been accidentally exposed to the internet due to poor security and/or intentional    exposure """
    def __init__(self, dork_string):
        self.dork_string = dork_string

    def search_with_dork(self):

